package com.softtek.javaweb.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.softtek.javaweb.JavaprojectApplication;
import com.softtek.javaweb.models.DayRegister;
import com.softtek.javaweb.models.MonthTotal;
import com.softtek.javaweb.models.Person;
import com.softtek.javaweb.services.DayRegisterService;
import com.softtek.javaweb.services.MonthTotalService;
import com.softtek.javaweb.services.PersonService;


@SpringBootTest(classes = JavaprojectApplication.class)
public class ServiceTest {
	@Autowired
	DayRegisterService dayRegisterService;
	@Autowired
	PersonService personService;
	@Autowired
	MonthTotalService monthTotalService;

	@Test
	public void testListSizeAllByDateAndName() {
		// setup
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = dateFormat.parse("2019-04-01");
			endDate = dateFormat.parse("2019-05-01");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		int expectedSize = 3;// list size expected

		// execute
		List<DayRegister> list;
		list = dayRegisterService.findAllByDateAndName("EDBG", startDate, endDate);

		// validate
		assertNotNull(list);
		assertEquals(expectedSize, list.size());

	}

	@Test
	public void testFindAllPersons() {
		// setup
		int expectedSize = 68;// list size expected PERSONS

		// execute
		List<Person> list;
		list = personService.getAll();

		// validate
		assertNotNull(list);
		assertEquals(expectedSize, list.size());

	}
	@Test
	public void testfindByMonthNumberOfHours() {
		// setup
		String expected = "43:56:7";// expected time
		String name="ZVDP";
		long month=4;
		
		// execute
		List<MonthTotal> list;
		list = monthTotalService.findByNameAndMonth(name, month);
		
		// validate
		assertEquals(expected, list.get(0).getHoursMonth());

	}
	
}
