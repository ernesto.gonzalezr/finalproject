package com.softtek.javaweb;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.softtek.javaweb.JavaprojectApplication;
import com.softtek.javaweb.models.MonthTotal;
import com.softtek.javaweb.services.MonthTotalService;

@SpringBootTest(classes = JavaprojectApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class Integration {
	@Autowired
	MonthTotalService monthTotalService;
	@LocalServerPort
	private int port;
	TestRestTemplate restTemplate = new TestRestTemplate();

	HttpHeaders headers = new HttpHeaders();

	@Test
	public void testsss() {
		// setup
		int month=3;

		// execute
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<Object> response = restTemplate.exchange("http://localhost:" + port + "/api/v1/period/"+month,
				HttpMethod.GET, null, Object.class);
		List<Object> list = (List<Object>) response.getBody();
		
		List<MonthTotal> listaService;
		listaService = monthTotalService.findByMonth(month);
		
		

		// validate
		assertEquals(listaService.size(), list.size());



	}

}
