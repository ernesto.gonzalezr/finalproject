<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
.button {
	background-color: #4CAF50;
	border: none;
	color: white;
	padding: 15px 32px;
	text-decoration: none;
	display: inline-block;
	font-size: 16px;
	margin: 4px 2px;
}

table {
	width: 100%;
	font-family: arial, sans-serif;
	border-collapse: collapse;
}

td, th {
	border: 1px solid #dddddd;
	padding: 8px;
}

tr:nth-child(even) {
	background-color: #dddddd;
}

div {
	width: 70%;
	margin-left: 15%;
	margin-right: 15%;
	text-align: center;
}
</style>
</head>

<body>
	<div>
		<table>
			<tr>
				<th>Name</th>
				<th>Total Hours (hh:mm:ss)</th>
				<th>Month</th>
				<th>Year</th>
			<tr>
				<c:forEach items="${list}" var="MonthTotal">
					<tr>
						<td>${MonthTotal.getPerson().getName()}</td>
						<td>${MonthTotal.getHoursMonth()}</td>
						<td>${MonthTotal.getMonth()}</td>
						<td>${MonthTotal.getYear()}</td>

					</tr>
				</c:forEach>
		</table>

		<a href="/" class="button">Menu</a>
	</div>
</body>

</html>