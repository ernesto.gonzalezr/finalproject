<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Java Project</title>
</head>
<style>
td, th {
	border: 1px solid #dddddd;
	padding: 8px;
}

table {
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

form {
	border: 1px solid black;
	width: 100%;
	table-layout: fixed;
}

div {
	border: 1px solid black;
	width: 70%;
	margin-left: 15%;
	margin-right: 15%;
	text-align: center;
}
</style>
<body>
	<div>
		<table>
			<tr>
				<th>Formularios</th>
			</tr>
			<tr>
				<td>
					<form action="/api/v1/usersView/" method="get">
						<br>
						<h4>Get total of hours of a user by month</h4>
						IS: <input type="text" name="name" placeholder="IS" required><br>
						<br> Month: <input type="text" name="mes"
							placeholder="Numeric" required><br> <br> <input
							type="submit" value="Search"><br> <br>
					</form>
				</td>
			</tr>

			<tr>

				<td>
					<form action="/api/v1/periodView/" method="get">
						<br>
						<h4>Get total of hours of all users by month</h4>
						Month: <input type="text" name="mes" placeholder="Numeric"
							required><br> <br> <input type="submit"
							value="Search"><br> <br>
					</form>
				</td>
			</tr>

			<tr>

				<td>
					<form action="/api/v1/periodDateView" method="post">
						<br>
						<h4>Get registers of all users between given dates</h4>
						Start date: <input type="text" name="startDate"
							placeholder="YYYY-MM-DD" required><br> <br> End
						Date: <input type="text" name="endDate" placeholder="YYYY-MM-DD"
							required><br> <br> <input type="submit"
							value="Search"><br> <br>
					</form>
				</td>
			</tr>
			<tr>

				<td>
					<form action="/api/v1/periodNameView" method="post">
						<br>
						<h4>Get registers of all users between given dates and IS</h4>
						IS: <input type="text" name="name" placeholder="IS" required><br>
						<br> Start date: <input type="text" name="startDate"
							placeholder="YYYY-MM-DD" required><br> <br> End
						Date: <input type="text" name="endDate" placeholder="YYYY-MM-DD"
							required><br> <br> <input type="submit"
							value="Search"><br> <br>
					</form>
				</td>

			</tr>
			<tr>

				<td>
					<form action="upload" method="post" enctype="multipart/form-data">
						<h4>Load new excel file</h4>
						<br> <input type="file" name="file" required /> <br> <br>
						<input type="submit" value="Upload" /> <br> <br>
					</form>
				</td>

			</tr>

		</table>
	</div>
</body>
</html>
