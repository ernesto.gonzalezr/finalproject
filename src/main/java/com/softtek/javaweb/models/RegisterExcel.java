package com.softtek.javaweb.models;

import java.util.Date;

public class RegisterExcel {

	int num;
	String department;
	String name;
	int id;
	Date date;
	String clockType;
	int deviceId;

	@Override
	public String toString() {
		return "RegistroExcel [num=" + num + ", department=" + department + ", name=" + name + ", id=" + id + ", date="
				+ date + ", clockType=" + clockType + ", deviceId=" + deviceId + "]";
	}

	public RegisterExcel() {
		
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getClockType() {
		return clockType;
	}

	public void setClockType(String clockType) {
		this.clockType = clockType;
	}

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}
}
