package com.softtek.javaweb.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class MonthTotal {
	@Id
	@Column(name = "idmonthtotal")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idMonthTotal;
	private String hoursMonth;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPerson")
	private Person person;

	private int month;
	private int year;

	public MonthTotal(long idMonthTotal, String hoursMonth, Person person) {

		this.idMonthTotal = idMonthTotal;
		this.hoursMonth = hoursMonth;
		this.person = person;
	}

	public MonthTotal() {

	}

	public long getIdMonthTotal() {
		return idMonthTotal;
	}

	public void setIdMonthTotal(long idMonthTotal) {
		this.idMonthTotal = idMonthTotal;
	}

	public String getHoursMonth() {
		return hoursMonth;
	}

	public void setHoursMonth(String hoursMonth) {
		this.hoursMonth = hoursMonth;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

}
