package com.softtek.javaweb.models;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
@Entity
public class DayRegister {
	@Id
	@Column(name="idregistro")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idRegister;
	
	@JsonManagedReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPerson")
	private Person person;
	
	private Time hoursDay;
	
	@Temporal(TemporalType.DATE)
	private Date date;
	private Time clockIn;
	private Time clockOut;
	
	
	public DayRegister() {
	}


	public long getIdRegister() {
		return idRegister;
	}


	public void setIdRegister(long idRegister) {
		this.idRegister = idRegister;
	}


	public Person getPerson() {
		return person;
	}


	public void setPerson(Person person) {
		this.person = person;
	}

	@JsonFormat(pattern="yyyy-MM-dd")
	public Time getHoursDay() {
		return hoursDay;
	}


	public void setHoursDay(Time hoursDay) {
		this.hoursDay = hoursDay;
	}

	@JsonFormat(pattern="HH:mm:ss")
	public Time getClockIn() {
		return clockIn;
	}


	public void setClockIn(Time clockIn) {
		this.clockIn = clockIn;
	}

	@JsonFormat(pattern="HH:mm:ss")
	public Time getClockOut() {
		return clockOut;
	}


	public void setClockOut(Time clockOut) {
		this.clockOut = clockOut;
	}


	public DayRegister(long idRegister, Person person, Time hoursDay, Time clockIn, Time clockOut, Date date) {

		this.idRegister = idRegister;
		this.person = person;
		this.hoursDay = hoursDay;
		this.clockIn = clockIn;
		this.clockOut = clockOut;
		this.date = date;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}
	
}
