package com.softtek.javaweb.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
@Entity
public class Person {
	@Id
	@Column(name = "idperson")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idPerson;
	private String name;
	@JsonBackReference
	@OneToMany(
			mappedBy = "person",
			cascade = CascadeType.ALL)
	private Set<DayRegister> registros = new HashSet<>();
	@JsonBackReference
	@OneToMany(
			mappedBy = "person",
			cascade = CascadeType.ALL)
	private Set<MonthTotal> monthTotal = new HashSet<>();
	
	
	public Set<MonthTotal> getMonthTotal() {
		return monthTotal;
	}
	public void setMonthTotal(Set<MonthTotal> monthTotal) {
		this.monthTotal = monthTotal;
	}
	public long getIdPerson() {
		return idPerson;
	}
	public void setIdPerson(long idPerson) {
		this.idPerson = idPerson;
	}
	public Set<DayRegister> getRegistros() {
		return registros;
	}
	public void setRegistros(Set<DayRegister> registros) {
		this.registros = registros;
	}
	public Person() {
	}
	public Person(long idPerson, String name) {

		this.idPerson = idPerson;
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void addRegister(DayRegister registro) {
		registros.add(registro);
		registro.setPerson(this);
	}
	
	public void removeRegister(DayRegister registro) {
		registros.remove(registro);
		registro.setPerson(null);
	}
	public void addMonthTotal(MonthTotal monthTotal) {
		this.monthTotal.add(monthTotal);
		monthTotal.setPerson(this);
	}
	
	public void removeMonthTotal(MonthTotal monthTotal) {
		this.monthTotal.remove(monthTotal);
		monthTotal.setPerson(null);
	}
}
