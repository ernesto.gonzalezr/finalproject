package com.softtek.javaweb.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.javaweb.models.DayRegister;
import com.softtek.javaweb.models.ErrorMessage;
import com.softtek.javaweb.services.DayRegisterService;

@Controller
@RequestMapping("api/v1")
public class ViewRegisterController {
	@Autowired
	DayRegisterService dayRegisterService;

	// 4.-a. POST : Envia el parametron de startDate y Enddate para obtener todos
	// los usuarios limitados
	// por la fecha de inicio y fin.
	@PostMapping("/periodDateView")
	public Object getAllByDate(@RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
			@RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) {
		List<DayRegister> registros = dayRegisterService.findAllByDateBetween(startDate, endDate);
		ModelAndView page;
		if (registros.isEmpty())
		{
			page = new ModelAndView("notFound");
		}
		else
		{
			page = new ModelAndView("DayRegisterView");
			page.addObject("list", registros);	
		}
		return page;

	}

	// 2.-a. POST: Envia los parámetros IS, startDate y EndDate para obtener todos
	// los usuarios limitados
	// por la fecha de inicio y fin.
	@PostMapping("/periodNameView")
	public Object getAllByDateAndName(@RequestParam("name") String name,
			@RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
			@RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) {
		List<DayRegister> registros = dayRegisterService.findAllByDateAndName(name, startDate, endDate);
		ModelAndView page;
		if (registros.isEmpty())
		{
			page = new ModelAndView("notFound");
		}
		else
		{
			page = new ModelAndView("DayRegisterView");
			page.addObject("list", registros);	
		}
		return page;
	}
}
