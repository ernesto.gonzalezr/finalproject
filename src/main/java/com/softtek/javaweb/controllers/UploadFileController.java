package com.softtek.javaweb.controllers;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.softtek.javaweb.services.ReadExcelService;

@Controller
public class UploadFileController {

	@Autowired
	private HttpServletRequest request;
	@Autowired
	private ReadExcelService readExcelService;

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public RedirectView HandleUpload(@RequestParam("file") MultipartFile file) throws IOException {
		if (!file.isEmpty()) {
			// save to uploads folder
			String filePath ="";
			try {
				String uploadsDir = "/uploads/";
				String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
				if (!new File(realPathtoUploads).exists()) {
					new File(realPathtoUploads).mkdir();
				}
				filePath = realPathtoUploads + file.getOriginalFilename();;
				File dest = new File(filePath);
				file.transferTo(dest);
				readExcelService.read(filePath);
			} catch (Exception e) {
				System.out.println("error: " + e);
			}
			
		}
		return new RedirectView("/");

	}
}
