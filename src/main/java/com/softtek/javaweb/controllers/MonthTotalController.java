package com.softtek.javaweb.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.javaweb.models.ErrorMessage;
import com.softtek.javaweb.models.MonthTotal;
import com.softtek.javaweb.services.MonthTotalService;

@RestController
@RequestMapping("api/v1")
public class MonthTotalController {

	@Autowired
	MonthTotalService monthTotalService;

	// 1.-a. GET : Obten las horas del IS del usuario del mes introducido
	@GetMapping(value = "users/{name}/{mes}")
	 public ResponseEntity<Object> getHoursByMount(@PathVariable String name, @PathVariable long mes) {
		List<MonthTotal> registros = monthTotalService.findByNameAndMonth(name, mes);
		if (registros.isEmpty())
			return new ResponseEntity<Object>(new ErrorMessage("No existe registro", new Date()), HttpStatus.OK);
			
		else
			return new ResponseEntity<Object>(registros, HttpStatus.OK);
	}

	// 3.- a.GET: Obten todos los registros de usuarios del mes introducido
	@GetMapping(value = "period/{mes}")
	 public ResponseEntity<Object> getRegisterByMonth(@PathVariable int mes) {
		List<MonthTotal> registros = monthTotalService.findByMonth(mes);
		if (registros.isEmpty())
			return new ResponseEntity<Object>(new ErrorMessage("No existe registro", new Date()), HttpStatus.OK);
			
		else
			return new ResponseEntity<Object>(registros, HttpStatus.OK);
			
	}

}
