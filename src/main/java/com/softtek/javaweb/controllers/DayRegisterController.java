package com.softtek.javaweb.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.javaweb.models.ErrorMessage;
import com.softtek.javaweb.models.DayRegister;
import com.softtek.javaweb.services.DayRegisterService;

@RestController
@RequestMapping("api/v1")
public class DayRegisterController {

	@Autowired
	DayRegisterService dayRegisterService;

	@GetMapping(value = "registros")
	 public ResponseEntity<Object> registros() {
		List<DayRegister> registros = dayRegisterService.getAll();
		if (registros.isEmpty())
			return new ResponseEntity<Object>(new ErrorMessage("No existe registro", new Date()), HttpStatus.OK);
			
		else
			return new ResponseEntity<Object>(registros, HttpStatus.OK);
	}

	// 4.-a. POST : Envia el parametron de startDate y Enddate para obtener todos
	// los usuarios limitados
	// por la fecha de inicio y fin.
	@PostMapping("periodDate/")
	 public ResponseEntity<Object> getAllByDate(@RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
			@RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) {
		List<DayRegister> registros = dayRegisterService.findAllByDateBetween(startDate, endDate);
		if (registros.isEmpty())
			return new ResponseEntity<Object>(new ErrorMessage("No existe registro", new Date()), HttpStatus.OK);
			
		else
			return new ResponseEntity<Object>(registros, HttpStatus.OK);
	}

	// 2.-a. POST: Envia los parámetros IS, startDate y EndDate para obtener todos
	// los usuarios limitados
	// por la fecha de inicio y fin.
	@PostMapping("periodName/")
	 public ResponseEntity<Object>getAllByDateAndName(@RequestParam("name") String name,
			@RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
			@RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) {
		List<DayRegister> registros = dayRegisterService.findAllByDateAndName(name, startDate, endDate);
		if (registros.isEmpty())
			return new ResponseEntity<Object>(new ErrorMessage("No existe registro", new Date()), HttpStatus.OK);
			
		else
			return new ResponseEntity<Object>(registros, HttpStatus.OK);
	}

}
