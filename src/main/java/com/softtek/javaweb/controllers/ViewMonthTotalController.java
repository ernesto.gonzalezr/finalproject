package com.softtek.javaweb.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.javaweb.models.ErrorMessage;
import com.softtek.javaweb.models.MonthTotal;
import com.softtek.javaweb.services.MonthTotalService;

@RestController
@RequestMapping("api/v1")
public class ViewMonthTotalController {

	@Autowired
	MonthTotalService monthTotalService;

	// 1.-a. GET : Obten las horas del IS del usuario del mes introducido
	@GetMapping(value = "usersView/")
	 public ModelAndView getHoursByMount(@RequestParam("name") String name, @RequestParam("mes") long mes) {
		List<MonthTotal> registros = monthTotalService.findByNameAndMonth(name, mes);
		ModelAndView page;
		if (registros.isEmpty())
		{
			page = new ModelAndView("notFound");
		}
		else
		{
			page = new ModelAndView("MonthTotalView");
			page.addObject("list", registros);	
		}
		return page;
	}

	// 3.- a.GET: Obten todos los registros de usuarios del mes introducido
	@GetMapping(value = "periodView/")
	public ModelAndView getRegisterByMonth(@RequestParam("mes") int mes) {
		List<MonthTotal> registros = monthTotalService.findByMonth(mes);
		ModelAndView page;
		if (registros.isEmpty())
		{
			page = new ModelAndView("notFound");
		}
		else
		{
			page = new ModelAndView("MonthTotalView");
			page.addObject("list", registros);	
		}
		return page;
	}

}
