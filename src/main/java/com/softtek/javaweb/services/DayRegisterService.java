package com.softtek.javaweb.services;

import java.util.Date;
import java.util.List;

import com.softtek.javaweb.models.DayRegister;


public interface
DayRegisterService {
	void save(DayRegister d);
	List<DayRegister> getAll();
	DayRegister findByIdRegister(Long id);
	List<DayRegister> findAllByDateBetween(Date startDate, Date endDate);
	List<DayRegister> findAllByDateAndName(String name, Date startDate, Date endDate);
}
