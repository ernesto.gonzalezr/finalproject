package com.softtek.javaweb.services;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.javaweb.models.DayRegister;
import com.softtek.javaweb.models.MonthTotal;
import com.softtek.javaweb.models.Person;
import com.softtek.javaweb.models.RegisterExcel;

@Service
public class ReadExcelServiceImpl implements ReadExcelService {
	@Autowired
	PersonService personService;

	@SuppressWarnings({ "deprecation" })
	@Override
	public void read(String filePath) {
		// reading excel
		InputStream ExcelFileToRead = null;
		Workbook workbook = null;
		try {
			ExcelFileToRead = new FileInputStream(filePath);
			// Getting the workbook instance for xls file
			workbook = new HSSFWorkbook(ExcelFileToRead);
			Sheet sheet = workbook.getSheetAt(0);
			DataFormatter dataFormatter = new DataFormatter();
			Row row;
			Cell cell;
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Iterator<Row> rows = sheet.rowIterator();
			List<RegisterExcel> listaRegistros = new ArrayList<>();
			rows.next();
			while (rows.hasNext()) {
				row = (Row) rows.next();
				Iterator<Cell> cells = row.cellIterator();
				RegisterExcel registroExcel = new RegisterExcel();
				int i = 0;
				while (cells.hasNext()) {
					cell = (Cell) cells.next();
					String cellValue = dataFormatter.formatCellValue(cell);
					switch (i) {
					case 0:
						registroExcel.setNum(Integer.parseInt(cellValue));
						break;
					case 2:
						registroExcel.setName(cellValue);
						break;
					case 3:
						registroExcel.setId(Integer.parseInt(cellValue));
						break;
					case 4:
						try {
							registroExcel.setDate(format.parse(cellValue));
						} catch (ParseException e) {
							e.printStackTrace();
						}
						break;
					case 6:
						registroExcel.setClockType(cellValue);
						break;
					case 7:
						registroExcel.setDeviceId(Integer.parseInt(cellValue));
						break;
					}
					listaRegistros.add(registroExcel);
					i++;
				}
			}
			ExcelFileToRead.close();
			List<String> nombresUnicos = //obtiene nombres unicos
					listaRegistros.stream()
					.map(RegisterExcel::getName)
					.distinct()
					.collect(Collectors.toList());

			SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
			HashMap<String, List<String>> map = new HashMap<String, List<String>>();
			for (String nombre : nombresUnicos) {
				List<String> fechaClock =
						listaRegistros.stream()
						.filter(r -> r.getName() == nombre)
						.map(f -> {return dateFormat.format(f.getDate());})
						.distinct()
						.collect(Collectors.toList());
				map.put(nombre, fechaClock);
			}
			
			map.forEach((k, v) -> {
				Person persona = new Person();
				v.stream().forEach(d -> {
					List<Date> checadas =
							listaRegistros.stream()
							.filter(o -> o.getName().equals(k))// name filter
							.filter(o -> dateFormat.format(o.getDate()).equals(d)).distinct()//filtra fechas unicas
							.map(RegisterExcel::getDate)// date
							.collect(Collectors.toList());
					if (checadas.size() > 1) {//ignora los dias que solo tienen un registro
						long diff = Collections.max(checadas).getTime() - Collections.min(checadas).getTime();//obtiene la diferencia entre el primer y ultimo registro del dia
						DayRegister registro = new DayRegister();
						try {
							registro.setDate(dateFormat.parse(dateFormat.format(Collections.max(checadas))));
						} catch (ParseException e) {
						}
						registro.setClockIn(new Time(Collections.min(checadas).getTime()));
						registro.setClockOut(new Time(Collections.max(checadas).getTime()));
						registro.setHoursDay(new Time(diff));
						persona.addRegister(registro); 
					}
				});

				Set<DayRegister> registros = persona.getRegistros();//se obtienen los registros de una person para obtener totales de mes
				registros.stream()
				.map(f -> {return f.getDate().getYear() + "-" + f.getDate().getMonth();})
				.distinct()
				.forEach(i -> {
					Long suma = registros.stream()
							.filter(f -> (f.getDate().getYear() + "-" + f.getDate().getMonth()).equals(i))
							.map(f -> {return f.getHoursDay().getTime();})
							.reduce(0l, (t1, t2) -> t1 + t2);//hace sumatoria
					
					long diffSeconds = suma / 1000 % 60;
					long diffMinutes = suma / (60 * 1000) % 60;
					long diffHours = suma / (60 * 60 * 1000);
					String[] parts = i.split("-");
					MonthTotal mes = new MonthTotal();
					mes.setMonth(Integer.valueOf(parts[1]) + 1);
					mes.setYear(Integer.valueOf(parts[0]) + 1900);
					mes.setHoursMonth(diffHours + ":" + diffMinutes + ":" + diffSeconds);
					persona.addMonthTotal(mes);
				});
				persona.setName(k);
				personService.save(persona);
				
			});


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
