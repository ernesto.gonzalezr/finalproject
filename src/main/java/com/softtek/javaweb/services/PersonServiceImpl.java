package com.softtek.javaweb.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.javaweb.models.Person;
import com.softtek.javaweb.repositories.PersonJPARepo;
@Service
public class PersonServiceImpl implements PersonService{

	@Autowired 
	PersonJPARepo personJPARepo; 
	@Override
	public void save(Person p) {
		personJPARepo.save(p);
	}
	@Override
	public List<Person> getAll() {
		return personJPARepo.findAll();
	}
	


	

}
