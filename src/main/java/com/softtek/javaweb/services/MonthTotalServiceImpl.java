package com.softtek.javaweb.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.javaweb.models.MonthTotal;
import com.softtek.javaweb.repositories.MonthTotalRepo;

@Service
public class MonthTotalServiceImpl implements MonthTotalService {

	@Autowired
	MonthTotalRepo monthTotalRepo;

	@Override
	public void save(MonthTotal m) {

		monthTotalRepo.save(m);
	}

	@Override
	public List<MonthTotal> findByNameAndMonth(String name, long mes) {
		return monthTotalRepo.findByNameAndMonth(name, mes);
	}

	@Override
	public List<MonthTotal> findByMonth(int mes) {
		return monthTotalRepo.findByMonth(mes);
	}

}
