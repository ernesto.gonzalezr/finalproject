package com.softtek.javaweb.services;

import java.util.List;

import com.softtek.javaweb.models.MonthTotal;

public interface MonthTotalService {
	void save(MonthTotal m);

	List<MonthTotal> findByNameAndMonth(String name, long mes);

	List<MonthTotal> findByMonth(int mes);

}
