package com.softtek.javaweb.services;


import java.util.List;


import com.softtek.javaweb.models.Person;

public interface PersonService {
	
	void save(Person p);
	List<Person> getAll();
}
