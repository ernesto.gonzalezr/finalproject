package com.softtek.javaweb.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.javaweb.models.DayRegister;
import com.softtek.javaweb.repositories.DayRegisterRepo;

@Service
public class DayRegisterServiceImpl implements DayRegisterService{

	@Autowired
	DayRegisterRepo dayRegisterRepo; 


	@Override
	public void save(DayRegister d) {
		dayRegisterRepo.save(d);
		
	}


	@Override
	public List<DayRegister> getAll() {
		return dayRegisterRepo.findAll();
	}


	@Override
	public List<DayRegister> findAllByDateBetween(Date startDate, Date endDate) {
		return dayRegisterRepo.findAllByDateBetween(startDate, endDate);
	}


	@Override
	public List<DayRegister> findAllByDateAndName(String name, Date startDate, Date endDate) {
		return dayRegisterRepo.findAllByDateAndName(name, startDate, endDate);
	}


	@Override
	public DayRegister findByIdRegister(Long id) {
		return dayRegisterRepo.findByIdRegister(id);
	}

	


	

}
