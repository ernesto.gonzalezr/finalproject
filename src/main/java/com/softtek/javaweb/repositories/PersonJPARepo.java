package com.softtek.javaweb.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.softtek.javaweb.models.Person;

public interface PersonJPARepo extends JpaRepository<Person,Long>{
	
	
}
