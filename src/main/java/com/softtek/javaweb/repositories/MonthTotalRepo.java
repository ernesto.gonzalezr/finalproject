package com.softtek.javaweb.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.softtek.javaweb.models.MonthTotal;

public interface MonthTotalRepo extends JpaRepository<MonthTotal,Long>{
	@Query(value = 
			"SELECT * from MonthTotal m,person p where m.idperson=p.idperson and p.name=?1 and m.month=?2",
			nativeQuery = true)
	List<MonthTotal> findByNameAndMonth(String name, long mes);
	List<MonthTotal> findByMonth(int mes);
	
	
}