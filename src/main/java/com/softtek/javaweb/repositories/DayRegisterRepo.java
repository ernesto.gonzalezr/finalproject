package com.softtek.javaweb.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.softtek.javaweb.models.DayRegister;
public interface DayRegisterRepo extends JpaRepository<DayRegister,Long>{

	List<DayRegister> findAllByDateBetween(Date startDate, Date endDate);
	@Query(value = 
			"SELECT * from DayRegister m,person p where m.idperson=p.idperson and p.name=?1	 and m.date between ?2 and ?3 ",
			nativeQuery = true)
	List<DayRegister> findAllByDateAndName(String name, Date startDate, Date endDate);
	
	DayRegister findByIdRegister(Long id);
}
